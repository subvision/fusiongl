﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System;
using FusionGL.Rendering;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using All = OpenTK.Graphics.OpenGL4.All;

namespace FusionGL.OpenGL
{
    public class OGLTexture : ITextureProxy
    {
        ~OGLTexture()
        {
            GL.DeleteTexture(_textureId);
            GL.DeleteFramebuffer(_frameBuffer);
            GL.DeleteRenderbuffer(_renderBuffer);
        }

        public void Finalize(Texture texture, TextureTarget textureTarget, int filter,
            PixelInternalFormat pixelInternalFormat, PixelFormat pixelFormat, bool clamp, int attachments)
        {
            _texture = texture;
            _textureId = GL.GenTexture();
            _textureTarget = textureTarget;

            GL.BindTexture(_textureTarget, _textureId);

            GL.TexParameter(_textureTarget, TextureParameterName.TextureMinFilter, filter);
            GL.TexParameter(_textureTarget, TextureParameterName.TextureMagFilter, filter);

            if (clamp)
            {
                GL.TexParameter(_textureTarget, TextureParameterName.TextureWrapS, (int) TextureWrapMode.ClampToEdge);
                GL.TexParameter(_textureTarget, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            }

            GL.TexImage2D(_textureTarget, 0, pixelInternalFormat, texture.Width, texture.Height, 0, pixelFormat, PixelType.Byte, texture.Data.Scan0);

            if (filter == (int) All.NearestMipmapNearest ||
                filter == (int) All.NearestMipmapLinear ||
                filter == (int) All.LinearMipmapNearest ||
                filter == (int) All.LinearMipmapLinear)
            {
                GL.GenerateMipmap((GenerateMipmapTarget) _textureTarget);
                float maxAnisotropy;
                GL.GetFloat((GetPName) ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out maxAnisotropy);
                GL.TexParameter(_textureTarget, (TextureParameterName) ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, (float) MathHelper.Clamp(0.0, 0.8, maxAnisotropy));
            }
            else
            {
                GL.TexParameter(_textureTarget, TextureParameterName.TextureBaseLevel, 0);
                GL.TexParameter(_textureTarget, TextureParameterName.TextureMaxLevel, 0);
            }


            if (attachments != 0)
            {
                int drawBuffers;

                bool hasDepth = false;
                if (attachments == (int) FramebufferAttachment.DepthAttachment)
                {
                    drawBuffers = (int) All.None;
                    hasDepth = true;
                }
                else
                {
                    drawBuffers = attachments;
                }

                if (attachments != (int) All.None)
                {
                    if (_frameBuffer == 0)
                    {
                        _frameBuffer = GL.GenFramebuffer();
                        GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffer);
                    }

                    GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, (FramebufferAttachment) attachments, _textureTarget, _textureId, 0);

                    if (!hasDepth)
                    {
                        _renderBuffer = GL.GenRenderbuffer();
                        GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, _renderBuffer);
                        GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent,
                            texture.Width, texture.Height);
                        GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, _renderBuffer);
                    }

                    GL.DrawBuffer((DrawBufferMode) drawBuffers);

                    if (GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer) !=
                        FramebufferErrorCode.FramebufferComplete)
                    {
                        throw new Exception("Framebuffer creation failed!");
                    }

                    GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                }
            }
        }

        public void Bind(int textureId)
        {
            GL.BindTexture(_textureTarget, _textureId);
        }

        public void BindAsRenderTarget()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, _frameBuffer);
            GL.Viewport(0, 0, _texture.Width, _texture.Height);
        }

        private int _textureId;
        private TextureTarget _textureTarget;
        private int _renderBuffer;
        private int _frameBuffer;
        private Texture _texture;
    }
}