﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System;
using FusionGL.Core;
using FusionGL.Rendering;
using SDL2;

namespace FusionGL.OpenGL
{
    public class OGLRenderer : IRenderingEngine
    {
        public IntPtr SDLRenderer { get; private set; }

        public OGLRenderer()
        {
            SDLRenderer = SDL.SDL_CreateRenderer((CoreEngine.Core.Window as OGLWindow).SDLWindow, -1, SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED | SDL.SDL_RendererFlags.SDL_RENDERER_PRESENTVSYNC);
            uint rMask = 0xff000000;
            uint gMask = 0x00ff0000;
            uint bMask = 0x0000ff00;
            uint aMask = 0x000000ff;
            //SDLsharp dont have BYTEORDER, maybe this will works good
            if (BitConverter.IsLittleEndian)
            {
                rMask = 0x000000ff;
                gMask = 0x0000ff00;
                bMask = 0x00ff0000;
                aMask = 0xff000000;
            }
            SDL.SDL_CreateRGBSurface(0, (CoreEngine.Core.Window as OGLWindow).Width, (CoreEngine.Core.Window as OGLWindow).Height, 32, rMask, gMask, bMask, aMask);
            Mesh.MeshProxyDef = new OGLMesh();
            Texture.TextureProxyDef = new OGLTexture();
        }

        public void Render(Entity entity)
        {

            //TODO: Implement rendering pipeline
            {
                //TODO: Render Meshes

                //TODO: Render To ShadowMaps

                //TODO: Blend lighting

                //TODO: Apply filters
            }
        }

        public void ApplyFilter(Shader filter, Texture source, Texture destination)
        {
            
        }
    }
}