﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System;
using SDL2;

namespace FusionGL.Rendering
{
    public class OGLWindow : Window
    {
        public OGLWindow(int width, int height, string title)
        {
            Width = width;
            Height = height;
            Title = title;
        }

        public IntPtr SDLWindow { get; private set; }

        public override void CreateWindow()
        {
            SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING);

            SDLWindow = SDL.SDL_CreateWindow(Title, SDL.SDL_WINDOWPOS_CENTERED, SDL.SDL_WINDOWPOS_CENTERED, Width,
                Height,
                SDL.SDL_WindowFlags.SDL_WINDOW_OPENGL);
        }

        public override void Update()
        {
            Input.Update();

            SDL.SDL_Event e;
            while (SDL.SDL_PollEvent(out e) != 0)
            {
                if (e.type == SDL.SDL_EventType.SDL_QUIT)
                {
                    CloseRequested = true;
                }

                if (e.type == SDL.SDL_EventType.SDL_MOUSEMOTION)
                {
                    Input.Mouse.MouseX = e.motion.x;
                    Input.Mouse.MouseY = e.motion.y;
                }

                if (e.type == SDL.SDL_EventType.SDL_KEYDOWN)
                {
                    var value = (int) e.key.keysym.scancode;

                    Input.Keyboard.KeyInputs[value] = 1;
                    Input.Keyboard.DownKeys[value] = 1;
                    Input.Keyboard.LastKey = value;
                }

                if (e.type == SDL.SDL_EventType.SDL_KEYUP)
                {
                    var value = (int) e.key.keysym.scancode;

                    Input.Keyboard.KeyInputs[value] = 0;
                    Input.Keyboard.UpKeys[value] = 1;
                }

                if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN)
                {
                    int value = e.button.button;

                    Input.Mouse.MouseInputs[value] = 1;
                    Input.Mouse.DownMouse[value] = 1;
                }

                if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONUP)
                {
                    int value = e.button.button;

                    Input.Mouse.MouseInputs[value] = 0;
                    Input.Mouse.UpMouse[value] = 1;
                }
            }
        }

        public override void SwapBuffers()
        {
        }

        public override void SetFullscreen(bool state)
        {
        }

        public override void BindAsRenderTarget()
        {
        }
    }
}