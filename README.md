# README #

A complete rewrite of Fusion3D now featuring support for various renderers, physics libraries and extended support for embedded languages. Engine aims for modularity and reliability of components.

### How to use this engine? ###

There is currently no front-end for handling multiple projects. Engine is currently under active development. Features might be subjects to change.

### Where could I access documentation? ###

Feel free to access http://fusiongl.aerobatic.io for engine documentation.

### License ###

This project is provided under Apache 2.0 (http://www.apache.org/licenses/LICENSE-2.0) license.

### Contribution guidelines ###

* Test your code before pull requests.
* Project follows ReSharper coding conventions.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact