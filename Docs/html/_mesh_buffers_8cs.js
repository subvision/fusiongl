var _mesh_buffers_8cs =
[
    [ "MeshBuffers", "_mesh_buffers_8cs.html#a545939e1ef44671647c9581ee63a6607", [
      [ "Position", "_mesh_buffers_8cs.html#a545939e1ef44671647c9581ee63a6607a52f5e0bc3859bc5f5e25130b6c7e8881", null ],
      [ "Texcoord", "_mesh_buffers_8cs.html#a545939e1ef44671647c9581ee63a6607ada074282ed9873b8f9e288912d2e1ab0", null ],
      [ "Normal", "_mesh_buffers_8cs.html#a545939e1ef44671647c9581ee63a6607a960b44c579bc2f6818d2daaf9e4c16f0", null ],
      [ "Tangent", "_mesh_buffers_8cs.html#a545939e1ef44671647c9581ee63a6607a541671cb1be09d76a84ba1a873ec3fc8", null ],
      [ "Index", "_mesh_buffers_8cs.html#a545939e1ef44671647c9581ee63a6607a88fa71f0a6e0dfedbb46d91cc0b37a50", null ],
      [ "NumBuffers", "_mesh_buffers_8cs.html#a545939e1ef44671647c9581ee63a6607a52b3f9afb50fb66b5de8b73d66bec17e", null ]
    ] ]
];