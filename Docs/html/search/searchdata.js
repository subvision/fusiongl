var indexSectionsWithContent =
{
  0: "abcdefghijklmnprstuvwz",
  1: "cegiklmpstw",
  2: "f",
  3: "acdegijklmnprstuw",
  4: "abcdefghiklmnprstuw",
  5: "abchiklmnpstz",
  6: "im",
  7: "ikmnptuw",
  8: "cdfghiklmprstuvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties"
};

