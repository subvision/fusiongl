var hierarchy =
[
    [ "FusionGL.Core.CoreEngine", "class_fusion_g_l_1_1_core_1_1_core_engine.html", null ],
    [ "Exception", null, [
      [ "FusionGL.Utils.GameNotSetException", "class_fusion_g_l_1_1_utils_1_1_game_not_set_exception.html", null ],
      [ "FusionGL.Utils.ProxyNotSetException", "class_fusion_g_l_1_1_utils_1_1_proxy_not_set_exception.html", null ]
    ] ],
    [ "FusionGL.Core.IGame", "interface_fusion_g_l_1_1_core_1_1_i_game.html", null ],
    [ "FusionGL.Core.InputDevices.IInputDevice", "interface_fusion_g_l_1_1_core_1_1_input_devices_1_1_i_input_device.html", [
      [ "FusionGL.Core.InputDevices.Keyboard", "class_fusion_g_l_1_1_core_1_1_input_devices_1_1_keyboard.html", null ],
      [ "FusionGL.Core.InputDevices.Mouse", "class_fusion_g_l_1_1_core_1_1_input_devices_1_1_mouse.html", null ]
    ] ],
    [ "FusionGL.Core.ILivable", "interface_fusion_g_l_1_1_core_1_1_i_livable.html", [
      [ "FusionGL.Core.Component", "class_fusion_g_l_1_1_core_1_1_component.html", [
        [ "FusionGL.Core.ScriptHost.LuaScript", "class_fusion_g_l_1_1_core_1_1_script_host_1_1_lua_script.html", null ]
      ] ],
      [ "FusionGL.Core.Entity", "class_fusion_g_l_1_1_core_1_1_entity.html", null ]
    ] ],
    [ "FusionGL.Rendering.IMeshProxy", "interface_fusion_g_l_1_1_rendering_1_1_i_mesh_proxy.html", null ],
    [ "FusionGL.Rendering.IndexedModel", "class_fusion_g_l_1_1_rendering_1_1_indexed_model.html", null ],
    [ "FusionGL.Core.Input", "class_fusion_g_l_1_1_core_1_1_input.html", null ],
    [ "FusionGL.Physics.IPhysicsEngine", "interface_fusion_g_l_1_1_physics_1_1_i_physics_engine.html", null ],
    [ "FusionGL.Rendering.IRenderingEngine", "interface_fusion_g_l_1_1_rendering_1_1_i_rendering_engine.html", null ],
    [ "FusionGL.Core.ComponentType.Logic.IScript", "interface_fusion_g_l_1_1_core_1_1_component_type_1_1_logic_1_1_i_script.html", [
      [ "FusionGL.Core.ScriptHost.LuaScript", "class_fusion_g_l_1_1_core_1_1_script_host_1_1_lua_script.html", null ]
    ] ],
    [ "FusionGL.Rendering.Material", "class_fusion_g_l_1_1_rendering_1_1_material.html", null ],
    [ "FusionGL.Rendering.Mesh", "class_fusion_g_l_1_1_rendering_1_1_mesh.html", null ],
    [ "FusionGL.Rendering.Shader", "class_fusion_g_l_1_1_rendering_1_1_shader.html", null ],
    [ "FusionGL.Core.Transform", "class_fusion_g_l_1_1_core_1_1_transform.html", null ],
    [ "FusionGL.Rendering.Window", "class_fusion_g_l_1_1_rendering_1_1_window.html", null ]
];