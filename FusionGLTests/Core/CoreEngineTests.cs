﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FusionGL.Core;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FusionGL.Physics;
using FusionGL.Rendering;
using OpenTK;

namespace FusionGL.Core.Tests
{
    [TestClass()]
    public class CoreEngineTests
    {
        [TestMethod()]
        public void InputTest()
        {
            Input input = new Input(null);
            input.DownKeys[(int) Utils.InputKeys.KeyA] = 1;
            Assert.AreEqual(1, input.DownKeys[(int) Utils.InputKeys.KeyA]);

            input.DownKeys[4] = 0;
            Assert.AreEqual(0, input.DownKeys[(int)Utils.InputKeys.KeyA]);
        }

        [TestMethod()]
        public void TransformTest()
        {
            Transform t1 = new Transform(new Vector3(1,2,3), new Quaternion(1,2,3,4), Vector3.One);

            Assert.AreEqual(2, t1.Position.Y);

            Transform t2 = new Transform();
            t2.SetParentTransform(t1);

            Assert.AreEqual(t1.GetTransformation(), t2.GetTransformation());
        }
    }
}