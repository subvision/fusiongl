﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System;
using FusionGL.Core;
using FusionGL.Core.ScriptHost;
using FusionGL.OpenGL;
using FusionGL.Physics;
using FusionGL.Rendering;
using FusionGL.Utils;

namespace WolfGL
{
    internal class Random42 : Component
    {
        public override void Init(Window window)
        {
            Console.WriteLine("42");
        }

        public override void Input(Input input, float delta)
        {
            if (input.Keyboard.DownKeys[(int) InputKeys.KeyS] == 1)
            {
                Console.WriteLine("You pressed 42!");
            }
        }

        public override void Update(IPhysicsEngine physicsEngine, float delta)
        {
        }

        public override void Render(Transform transform, IRenderingEngine renderingEngine, Shader shader)
        {
        }
    }

    internal class TestGame : IGame
    {
        private static readonly string code = @"
function Init(window)
    Console.WriteLine('Ahoy from Lua')
end
";
        private Entity _e;

        public void Init(Window window)
        {
            Console.WriteLine("Init()");
            _e = new Entity();
            _e.AddComponent(new Random42());
            _e.AddComponent(new LuaScript(code));
            _e.Init(window);
        }

        public void Input(Input input, float delta)
        {
            //Console.WriteLine("{0}", input.DownKeys[(int) Utils.InputKeys.KeyA]);

            if (input.Keyboard.DownKeys[(int) InputKeys.KeyW] == 1)
            {
                Console.WriteLine("Key W pressed!");
            }

            _e.Input(input, delta);
        }

        public void Update(IPhysicsEngine physicsEngine, float delta)
        {
        }

        public void Render(IRenderingEngine renderingEngine)
        {
        }
    }

    internal class TestPhysics : IPhysicsEngine
    {
        public void Update(float delta)
        {
            //TODO: Implement basic physics or use library
            // JitterPhysics..
        }
    }

    public static class Program
    {
        private static void Main(string[] args)
        {
            var test = new CoreEngine(new OGLWindow(800, 600, "Test Game"), new TestPhysics(), new OGLRenderer(), 60.0d);

            test.SetGame(new TestGame());
            test.Start();
        }
    }
}