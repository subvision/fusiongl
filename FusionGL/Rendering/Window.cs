﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using FusionGL.Core;
using OpenTK;

namespace FusionGL.Rendering
{
    public abstract class Window
    {
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        public string Title { get; protected set; }
        public Input Input { get; internal set; }
        public bool CloseRequested { get; protected set; }
        public abstract void CreateWindow();
        public abstract void Update();
        public abstract void BindAsRenderTarget();

        public float GetAspect()
        {
            return Width/(float) Height;
        }

        public abstract void SwapBuffers();
        public abstract void SetFullscreen(bool state);
        public Vector2 GetCenter() => new Vector2(Width/2.0f, Height/2.0f);
    }
}