﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System;
using System.Drawing.Imaging;

namespace FusionGL.Rendering
{
    public class Texture
    {
        public Texture(int width, int height, BitmapData data)
        {
            Width = width;
            Height = height;
            Data = data;
        }

        public Texture(BitmapData bd) : this(bd.Width, bd.Height, bd)
        {
            
        }

        public static ITextureProxy TextureProxyDef;

        public int Width { get; internal set; }
        public int Height { get; internal set; }
        public BitmapData Data { get; internal set; }
    }
}