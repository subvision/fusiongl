﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using FusionGL.Utils;

namespace FusionGL.Rendering
{
    public class Mesh
    {
        // ReSharper disable once UnassignedField.Global
        public static IMeshProxy MeshProxyDef;

        public Mesh(IndexedModel model, Material material, bool lazy = false)
        {
            Model = model;
            Material = material;

            VAB = new uint[(int) MeshBuffers.NumBuffers];

            if (MeshProxyDef == null && !lazy)
            {
                throw new ProxyNotSetException("Mesh Proxy is not set!");
            }
            if (!lazy)
            {
                MeshProxyDef.Init(this);
            }
        }

        public Material Material { get; set; }
        public IndexedModel Model { get; set; }

        public int DrawCount { get; private set; }
        public uint VAO { get; set; }
        public uint[] VAB { get; set; }

        public void Draw()
        {
            MeshProxyDef.Draw(this);
        }
    }
}