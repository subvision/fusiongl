﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using OpenTK;

namespace FusionGL.Rendering
{
    /// <summary>
    /// Defines model used by engine.
    /// </summary>
    public class IndexedModel
    {
        public List<int> Indices;
        public List<Vector3> Normals;
        public List<Vector3> Positions;
        public List<Vector3> Tangents;
        public List<Vector2> TexCoords;

        public IndexedModel(List<int> indices, List<Vector3> normals, List<Vector3> positions, List<Vector3> tangents,
            List<Vector2> texCoords)
        {
            Indices = indices;
            Normals = normals;
            Positions = positions;
            Tangents = tangents;
            TexCoords = texCoords;
        }

        public void AddFace(int v0, int v1, int v2)
        {
            Indices.Add(v0);
            Indices.Add(v1);
            Indices.Add(v2);
        }

        public bool IsValid()
        {
            return Positions.Count == TexCoords.Count
                   && TexCoords.Count == Normals.Count
                   && Normals.Count == Tangents.Count;
        }

        public IndexedModel Finalize(bool normalsFlat = true)
        {
            if (IsValid())
            {
                return this;
            }

            if (TexCoords.Count == 0)
            {
                for (var i = 0; i < Positions.Count; i++)
                {
                    TexCoords.Add(Vector2.Zero);
                }
            }

            if (Normals.Count == 0)
            {
                CalcNormals(normalsFlat);
            }

            if (Tangents.Count == 0)
            {
                CalcTangents();
            }

            return this;
        }

        private void CalcTangents()
        {
            Tangents.Clear();
            Tangents.Capacity = Positions.Count;

            for (var i = 0; i < Positions.Count; i++)
            {
                Tangents.Add(Vector3.Zero);
            }

            for (var i = 0; i < Indices.Count; i += 3)
            {
                var i0 = Indices[i];
                var i1 = Indices[i + 1];
                var i2 = Indices[i + 2];

                var edge1 = Positions[i1] - Positions[i0];
                var edge2 = Positions[i2] - Positions[i0];

                var deltaU1 = TexCoords[i1].X - TexCoords[i0].X;
                var deltaU2 = TexCoords[i2].X - TexCoords[i0].X;
                var deltaV1 = TexCoords[i1].Y - TexCoords[i0].Y;
                var deltaV2 = TexCoords[i2].Y - TexCoords[i0].Y;

                var dividend = deltaU1*deltaV2 - deltaU2*deltaV1;
                var f = dividend == 0.0f ? 0.0f : 1.0f/dividend;

                var tangent = Vector3.Zero;

                tangent.X = f*(deltaV2*edge1.X - deltaV1*edge2.X);
                tangent.Y = f*(deltaV2*edge1.Y - deltaV1*edge2.Y);
                tangent.Z = f*(deltaV2*edge1.Z - deltaV1*edge2.Z);

                Tangents[i0] += tangent;
                Tangents[i1] += tangent;
                Tangents[i2] += tangent;
            }
        }

        private void CalcNormals(bool normalsFlat = true)
        {
            Normals.Clear();
            Normals.Capacity = Positions.Count;

            for (var i = 0; i < Positions.Count; i++)
            {
                Normals.Add(Vector3.Zero);
            }

            if (normalsFlat)
            {
                for (var i = 0; i < Indices.Count; i += 3)
                {
                    var i0 = Indices[i];
                    var i1 = Indices[i + 1];
                    var i2 = Indices[i + 2];

                    var v1 = Positions[i1] - Positions[i0];
                    var v2 = Positions[i2] - Positions[i0];

                    var normal = Vector3.Cross(v1, v2).Normalized();

                    Normals[i0] += normal;
                    Normals[i1] += normal;
                    Normals[i2] += normal;
                }
            }
            else
            {
                //TODO: Vertex normals
                throw new NotImplementedException("Vertex normals aren't implemented yet!");
            }
        }
    }
}