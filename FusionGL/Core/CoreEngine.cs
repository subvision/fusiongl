﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System;
using System.Diagnostics;
using System.Threading;
using FusionGL.Physics;
using FusionGL.Rendering;
using FusionGL.Utils;

namespace FusionGL.Core
{
    /// <summary>
    /// Defines the core part of this engine. Handles various sub-systems and manages application lifecycle.
    /// </summary>
    public class CoreEngine
    {
        public CoreEngine(Window window, IPhysicsEngine physicsEngine, IRenderingEngine renderingEngine,
            double frameRate)
        {
            Core = this;

            Window = window;
            PhysicsEngine = physicsEngine;
            RenderingEngine = renderingEngine;
            FrameTime = 1.0d/frameRate;
            Input = new Input();

            Window.Input = Input;
            Window.CreateWindow();
        }

        public IGame Game { get; set; }
        public double FrameTime { get; }
        public Window Window { get; }
        public Input Input { get; }
        public IRenderingEngine RenderingEngine { get; }
        public IPhysicsEngine PhysicsEngine { get; }
        public static CoreEngine Core { get; private set; }
        public bool IsRunning { get; set; }

        public void Start()
        {
            if (Game == null)
                throw new GameNotSetException("*PANIC* Game isn't set. Pass Game impl into SetEngine method!");
            IsRunning = true;

            Game.Init(Window);

            var timer = new Stopwatch();
            timer.Start();
            var lastTime = 0.0d;
            var unprocessedTime = 0.0d;
            var frameCounter = 0.0d;
            var frames = 0;

            while (IsRunning)
            {
                var render = false;

                var startTime = timer.ElapsedMilliseconds/1000.0d;
                var passedTime = startTime - lastTime;
                lastTime = startTime;

                unprocessedTime += passedTime;
                frameCounter += passedTime;

                if (frameCounter >= 1.0d)
                {
                    Console.WriteLine("Ping: {0} ms (FPS: {1})", frameCounter*1000.0/frames,
                        1000.0d/(frameCounter*1000.0/frames));

                    frameCounter = 0;
                    frames = 0;
                }

                while (unprocessedTime > FrameTime)
                {
                    Window.Update();

                    if (Window.CloseRequested)
                        IsRunning = false;

                    Game.Input(Input, (float) FrameTime);

                    Game.Update(PhysicsEngine, (float) FrameTime);

                    render = true;

                    unprocessedTime -= FrameTime;
                }

                if (render)
                {
                    Game.Render(RenderingEngine);

                    Window.SwapBuffers();

                    frames++;
                }
                else
                {
                    Thread.Sleep(1);
                }
            }
        }

        public void Stop()
        {
            IsRunning = false;
        }

        public void SetGame(IGame game)
        {
            Game = game;
        }
    }
}