﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System.Collections.Generic;

namespace FusionGL.Core
{
    /// <summary>
    /// Used for caching objects.
    /// </summary>
    public static class UniqueObject
    {
        private static readonly Dictionary<string, object> _objects = new Dictionary<string, object>();

        public static object Spawn(string name)
        {
            if (_objects.ContainsKey(name))
            {
                return _objects[name];
            }
            return null;
        }

        public static object Spawn(string name, object obj)
        {
            if (_objects.ContainsKey(name))
            {
                return _objects[name];
            }

            _objects.Add(name, obj);
            return obj;
        }
    }
}