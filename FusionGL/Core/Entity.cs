﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System.Collections.Generic;
using FusionGL.Physics;
using FusionGL.Rendering;

namespace FusionGL.Core
{
    /// <summary>
    /// Implements living entity handled by <see cref="CoreEngine"/>.
    /// </summary>
    public sealed class Entity : ILivable
    {
        public Entity(Transform transform = default(Transform))
        {
            Transform = transform;
            Children = new List<Entity>();
            Components = new List<Component>();
        }

        public List<Entity> Children { get; }
        public List<Component> Components { get; }
        public Entity Parent { get; private set; }
        public Transform Transform { get; set; }

        public void Init(Window window)
        {
            foreach (var child in Children)
            {
                child.Init(window);
            }

            foreach (var component in Components)
            {
                component.Init(window);
            }
        }

        public void Input(Input input, float delta)
        {
            foreach (var child in Children)
            {
                child.Input(input, delta);
            }

            foreach (var component in Components)
            {
                component.Input(input, delta);
            }
        }

        public void Update(IPhysicsEngine physicsEngine, float delta)
        {
            foreach (var child in Children)
            {
                child.Update(physicsEngine, delta);
            }

            foreach (var component in Components)
            {
                component.Update(physicsEngine, delta);
            }
        }

        public void Render(Transform transform, IRenderingEngine renderingEngine, Shader shader)
        {
            foreach (var child in Children)
            {
                child.Render(transform, renderingEngine, shader);
            }

            foreach (var component in Components)
            {
                component.Render(transform, renderingEngine, shader);
            }
        }

        public Entity AddChild(Entity entity)
        {
            entity.Parent = this;
            Children.Add(entity);
            return this;
        }

        public Entity AddComponent(Component component)
        {
            component.Host = this;
            Components.Add(component);
            return this;
        }
    }
}