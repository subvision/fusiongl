﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
namespace FusionGL.Core.InputDevices
{
    public class Mouse : IInputDevice
    {
        private static readonly int NumMouseButtons = 256;

        public Mouse()
        {
            MouseInputs = new int[NumMouseButtons];
            DownMouse = new int[NumMouseButtons];
            UpMouse = new int[NumMouseButtons];
        }

        public int[] MouseInputs { get; }
        public int[] DownMouse { get; }
        public int[] UpMouse { get; }
        public int MouseX { get; set; }
        public int MouseY { get; set; }

        public void Update()
        {
            for (var i = 0; i < NumMouseButtons; i++)
            {
                DownMouse[i] = 0;
                UpMouse[i] = 0;
            }
        }
    }
}