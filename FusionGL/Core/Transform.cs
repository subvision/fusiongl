﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using OpenTK;

namespace FusionGL.Core
{
    /// <summary>
    /// Handles object transformation.
    /// </summary>
    public class Transform
    {
        private bool _initOldStuff;

        private Vector3 _oldPosition;
        private Quaternion _oldRotation;
        private Vector3 _oldScale;
        private Matrix4 _parentMatrix;

        private Transform _parentTransform;

        public Transform()
        {
            Position = Vector3.Zero;
            Rotation = Quaternion.Identity;
            Scale = Vector3.One;

            _parentTransform = null;
            _parentMatrix = Matrix4.Identity;
            _initOldStuff = false;
        }

        public Transform(Vector3 position, Quaternion rotation, Vector3 scale) : this()
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }

        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }
        public Vector3 Scale { get; set; }

        public void Update()
        {
            if (_initOldStuff)
            {
                _oldPosition = Position;
                _oldRotation = Rotation;
                _oldScale = Scale;
            }
            else
            {
                _oldPosition = Position + Vector3.One;
                _oldRotation = Rotation*0.5f;
                _oldScale = Scale + Vector3.One;
                _initOldStuff = true;
            }
        }

        public void SetParentTransform(Transform parentTransform)
        {
            _parentTransform = parentTransform;
        }

        private Matrix4 GetParentMatrix()
        {
            if (_parentTransform != null && _parentTransform.HasChanged())
            {
                _parentMatrix = _parentTransform.GetTransformation();
            }

            return _parentMatrix;
        }

        public Matrix4 GetTransformation()
        {
            var translationMatrix = Matrix4.CreateTranslation(Position);
            var scaleMatrix = Matrix4.CreateScale(Scale);
            var rotationMatrix = Matrix4.CreateFromQuaternion(Rotation);

            var result = translationMatrix*rotationMatrix*scaleMatrix;

            return GetParentMatrix()*result;
        }

        private bool HasChanged()
        {
            if (_parentTransform != null && _parentTransform.HasChanged()) return true;

            if (Position != _oldPosition) return true;

            if (Rotation != _oldRotation) return true;

            if (Scale != _oldScale) return true;

            return false;
        }
    }
}