﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using FusionGL.Core.ComponentType.Logic;
using FusionGL.Physics;
using FusionGL.Rendering;
using NLua;
using NLua.Event;

namespace FusionGL.Core.ScriptHost
{
    /// <summary>
    /// Implements scripting via Lua language.
    /// </summary>
    public class LuaScript : Component, IScript
    {
        private static readonly string imports = @"
import ('FusionGL','Core')
import ('FusionGL','Rendering')
import ('FusionGL','Physics')
import ('FusionGL','Utils')
import ('FusionGL','Libs')
import ('OpenTK')
import ('System')
";

        public LuaScript(string code)
        {
            Code = code;
            State = new Lua();
            State.LoadCLRPackage();
            State.DoString(imports);
            State.DoString(code);
            State.HookException += State_HookException;
        }

        private Lua State { get; }
        private string Code { get; set; }

        public object[] CallEvent(string name, params object[] data)
        {
            var func = State[name];
            if (func == null) return null;

            var call = func as LuaFunction;

            if (call != null) return call.Call(data);
            return null;
        }

        private void State_HookException(object sender, HookExceptionEventArgs e)
        {
            throw new ScriptExecutionException(e.ToString());
        }

        public override void Init(Window window)
        {
            CallEvent("Init", window);
        }

        public override void Input(Input input, float delta)
        {
            CallEvent("Input", input, delta);
        }

        public override void Update(IPhysicsEngine physicsEngine, float delta)
        {
            CallEvent("Update", physicsEngine, delta);
        }

        public override void Render(Transform transform, IRenderingEngine renderingEngine, Shader shader)
        {
            CallEvent("Render", transform, renderingEngine, shader);
        }
    }
}