﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using FusionGL.Physics;
using FusionGL.Rendering;

namespace FusionGL.Core
{
    /// <summary>
    /// Defines components used by Entity.
    /// </summary>
    public abstract class Component : ILivable
    {
        public Entity Host { get; internal set; }
        public abstract void Init(Window window);
        public abstract void Input(Input input, float delta);
        public abstract void Update(IPhysicsEngine physicsEngine, float delta);
        public abstract void Render(Transform transform, IRenderingEngine renderingEngine, Shader shader);
    }
}