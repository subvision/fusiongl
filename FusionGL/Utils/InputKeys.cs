﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
namespace FusionGL.Utils
{
    public enum InputKeys
    {
        KeyUnknown = 0,

        /**
		 *  \name Usage page 0x07
		 *
		 *  These values are from usage page 0x07 (USB keyboard page).
		 */
        /*@{*/

        KeyA = 4,
        KeyB = 5,
        KeyC = 6,
        KeyD = 7,
        KeyE = 8,
        KeyF = 9,
        KeyG = 10,
        KeyH = 11,
        KeyI = 12,
        KeyJ = 13,
        KeyK = 14,
        KeyL = 15,
        KeyM = 16,
        KeyN = 17,
        KeyO = 18,
        KeyP = 19,
        KeyQ = 20,
        KeyR = 21,
        KeyS = 22,
        KeyT = 23,
        KeyU = 24,
        KeyV = 25,
        KeyW = 26,
        KeyX = 27,
        KeyY = 28,
        KeyZ = 29,

        Key1 = 30,
        Key2 = 31,
        Key3 = 32,
        Key4 = 33,
        Key5 = 34,
        Key6 = 35,
        Key7 = 36,
        Key8 = 37,
        Key9 = 38,
        Key0 = 39,

        KeyReturn = 40,
        KeyEscape = 41,
        KeyBackspace = 42,
        KeyTab = 43,
        KeySpace = 44,

        KeyMinus = 45,
        KeyEquals = 46,
        KeyLeftbracket = 47,
        KeyRightbracket = 48,
        KeyBackslash = 49, /**< Located at the lower left of the return
									  *   key on ISO keyboards and at the right end
									  *   of the QWERTY row on ANSI keyboards.
									  *   Produces REVERSE SOLIDUS (backslash) and
									  *   VERTICAL LINE in a US layout, REVERSE
									  *   SOLIDUS and VERTICAL LINE in a UK Mac
									  *   layout, NUMBER SIGN and TILDE in a UK
									  *   Windows layout, DOLLAR SIGN and POUND SIGN
									  *   in a Swiss German layout, NUMBER SIGN and
									  *   APOSTROPHE in a German layout, GRAVE
									  *   ACCENT and POUND SIGN in a French Mac
									  *   layout, and ASTERISK and MICRO SIGN in a
									  *   French Windows layout.
									  */
        KeyNonushash = 50, /**< ISO USB keyboards actually use this code
									  *   instead of 49 for the same key, but all
									  *   OSes I've seen treat the two codes
									  *   identically. So, as an implementor, unless
									  *   your keyboard generates both of those
									  *   codes and your OS treats them differently,
									  *   you should generate KEY_BACKSLASH
									  *   instead of this code. As a user, you
									  *   should not rely on this code because SDL
									  *   will never generate it with most (all?)
									  *   keyboards.
									  */
        KeySemicolon = 51,
        KeyApostrophe = 52,
        KeyGrave = 53, /**< Located in the top left corner (on both ANSI
								  *   and ISO keyboards). Produces GRAVE ACCENT and
								  *   TILDE in a US Windows layout and in US and UK
								  *   Mac layouts on ANSI keyboards, GRAVE ACCENT
								  *   and NOT SIGN in a UK Windows layout, SECTION
								  *   SIGN and PLUS-MINUS SIGN in US and UK Mac
								  *   layouts on ISO keyboards, SECTION SIGN and
								  *   DEGREE SIGN in a Swiss German layout (Mac:
								  *   only on ISO keyboards), CIRCUMFLEX ACCENT and
								  *   DEGREE SIGN in a German layout (Mac: only on
								  *   ISO keyboards), SUPERSCRIPT TWO and TILDE in a
								  *   French Windows layout, COMMERCIAL AT and
								  *   NUMBER SIGN in a French Mac layout on ISO
								  *   keyboards, and LESS-THAN SIGN and GREATER-THAN
								  *   SIGN in a Swiss German, German, or French Mac
								  *   layout on ANSI keyboards.
								  */
        KeyComma = 54,
        KeyPeriod = 55,
        KeySlash = 56,

        KeyCapslock = 57,

        KeyF1 = 58,
        KeyF2 = 59,
        KeyF3 = 60,
        KeyF4 = 61,
        KeyF5 = 62,
        KeyF6 = 63,
        KeyF7 = 64,
        KeyF8 = 65,
        KeyF9 = 66,
        KeyF10 = 67,
        KeyF11 = 68,
        KeyF12 = 69,

        KeyPrintscreen = 70,
        KeyScrolllock = 71,
        KeyPause = 72,
        KeyInsert = 73, /**< insert on PC, help on some Mac keyboards (but
									   does send code 73, not 117) */
        KeyHome = 74,
        KeyPageup = 75,
        KeyDelete = 76,
        KeyEnd = 77,
        KeyPagedown = 78,
        KeyRight = 79,
        KeyLeft = 80,
        KeyDown = 81,
        KeyUp = 82,

        KeyNumlockclear = 83, /**< num lock on PC, clear on Mac keyboards
										 */
        KeyKpDivide = 84,
        KeyKpMultiply = 85,
        KeyKpMinus = 86,
        KeyKpPlus = 87,
        KeyKpEnter = 88,
        KeyKp1 = 89,
        KeyKp2 = 90,
        KeyKp3 = 91,
        KeyKp4 = 92,
        KeyKp5 = 93,
        KeyKp6 = 94,
        KeyKp7 = 95,
        KeyKp8 = 96,
        KeyKp9 = 97,
        KeyKp0 = 98,
        KeyKpPeriod = 99,

        KeyNonusbackslash = 100, /**< This is the additional key that ISO
											*   keyboards have over ANSI ones,
											*   located between left shift and Y.
											*   Produces GRAVE ACCENT and TILDE in a
											*   US or UK Mac layout, REVERSE SOLIDUS
											*   (backslash) and VERTICAL LINE in a
											*   US or UK Windows layout, and
											*   LESS-THAN SIGN and GREATER-THAN SIGN
											*   in a Swiss German, German, or French
											*   layout. */
        KeyApplication = 101, /**< windows contextual menu, compose */
        KeyPower = 102, /**< The USB document says this is a status flag,
								   *   not a physical key - but some Mac keyboards
								   *   do have a power key. */
        KeyKpEquals = 103,
        KeyF13 = 104,
        KeyF14 = 105,
        KeyF15 = 106,
        KeyF16 = 107,
        KeyF17 = 108,
        KeyF18 = 109,
        KeyF19 = 110,
        KeyF20 = 111,
        KeyF21 = 112,
        KeyF22 = 113,
        KeyF23 = 114,
        KeyF24 = 115,
        KeyExecute = 116,
        KeyHelp = 117,
        KeyMenu = 118,
        KeySelect = 119,
        KeyStop = 120,
        KeyAgain = 121, /**< redo */
        KeyUndo = 122,
        KeyCut = 123,
        KeyCopy = 124,
        KeyPaste = 125,
        KeyFind = 126,
        KeyMute = 127,
        KeyVolumeup = 128,
        KeyVolumedown = 129,
        /* not sure whether there's a reason to enable these */
        /*     KEY_LOCKINGCAPSLOCK = 130,  */
        /*     KEY_LOCKINGNUMLOCK = 131, */
        /*     KEY_LOCKINGSCROLLLOCK = 132, */
        KeyKpComma = 133,
        KeyKpEqualsas400 = 134,

        KeyInternational1 = 135, /**< used on Asian keyboards, see
												footnotes in USB doc */
        KeyInternational2 = 136,
        KeyInternational3 = 137, /**< Yen */
        KeyInternational4 = 138,
        KeyInternational5 = 139,
        KeyInternational6 = 140,
        KeyInternational7 = 141,
        KeyInternational8 = 142,
        KeyInternational9 = 143,
        KeyLang1 = 144, /**< Hangul/English toggle */
        KeyLang2 = 145, /**< Hanja conversion */
        KeyLang3 = 146, /**< Katakana */
        KeyLang4 = 147, /**< Hiragana */
        KeyLang5 = 148, /**< Zenkaku/Hankaku */
        KeyLang6 = 149, /**< reserved */
        KeyLang7 = 150, /**< reserved */
        KeyLang8 = 151, /**< reserved */
        KeyLang9 = 152, /**< reserved */

        KeyAlterase = 153, /**< Erase-Eaze */
        KeySysreq = 154,
        KeyCancel = 155,
        KeyClear = 156,
        KeyPrior = 157,
        KeyReturn2 = 158,
        KeySeparator = 159,
        KeyOut = 160,
        KeyOper = 161,
        KeyClearagain = 162,
        KeyCrsel = 163,
        KeyExsel = 164,

        KeyKp00 = 176,
        KeyKp000 = 177,
        KeyThousandsseparator = 178,
        KeyDecimalseparator = 179,
        KeyCurrencyunit = 180,
        KeyCurrencysubunit = 181,
        KeyKpLeftparen = 182,
        KeyKpRightparen = 183,
        KeyKpLeftbrace = 184,
        KeyKpRightbrace = 185,
        KeyKpTab = 186,
        KeyKpBackspace = 187,
        KeyKpA = 188,
        KeyKpB = 189,
        KeyKpC = 190,
        KeyKpD = 191,
        KeyKpE = 192,
        KeyKpF = 193,
        KeyKpXor = 194,
        KeyKpPower = 195,
        KeyKpPercent = 196,
        KeyKpLess = 197,
        KeyKpGreater = 198,
        KeyKpAmpersand = 199,
        KeyKpDblampersand = 200,
        KeyKpVerticalbar = 201,
        KeyKpDblverticalbar = 202,
        KeyKpColon = 203,
        KeyKpHash = 204,
        KeyKpSpace = 205,
        KeyKpAt = 206,
        KeyKpExclam = 207,
        KeyKpMemstore = 208,
        KeyKpMemrecall = 209,
        KeyKpMemclear = 210,
        KeyKpMemadd = 211,
        KeyKpMemsubtract = 212,
        KeyKpMemmultiply = 213,
        KeyKpMemdivide = 214,
        KeyKpPlusminus = 215,
        KeyKpClear = 216,
        KeyKpClearentry = 217,
        KeyKpBinary = 218,
        KeyKpOctal = 219,
        KeyKpDecimal = 220,
        KeyKpHexadecimal = 221,

        KeyLctrl = 224,
        KeyLshift = 225,
        KeyLalt = 226, /**< alt, option */
        KeyLgui = 227, /**< windows, command (apple), meta */
        KeyRctrl = 228,
        KeyRshift = 229,
        KeyRalt = 230, /**< alt gr, option */
        KeyRgui = 231, /**< windows, command (apple), meta */

        KeyMode = 257, /**< I'm not sure if this is really not covered
									 *   by any of the above, but since there's a
									 *   special KMOD_MODE for it I'm adding it here
									 */

        /*@}*/ /*Usage page 0x07*/

        /**
		 *  \name Usage page 0x0C
		 *
		 *  These values are mapped from usage page 0x0C (USB consumer page).
		 */
        /*@{*/

        KeyAudionext = 258,
        KeyAudioprev = 259,
        KeyAudiostop = 260,
        KeyAudioplay = 261,
        KeyAudiomute = 262,
        KeyMediaselect = 263,
        KeyWww = 264,
        KeyMail = 265,
        KeyCalculator = 266,
        KeyComputer = 267,
        KeyAcSearch = 268,
        KeyAcHome = 269,
        KeyAcBack = 270,
        KeyAcForward = 271,
        KeyAcStop = 272,
        KeyAcRefresh = 273,
        KeyAcBookmarks = 274,

        /*@}*/ /*Usage page 0x0C*/

        /**
		 *  \name Walther keys
		 *
		 *  These are values that Christian Walther added (for mac keyboard?).
		 */
        /*@{*/

        KeyBrightnessdown = 275,
        KeyBrightnessup = 276,
        KeyDisplayswitch = 277, /**< display mirroring/dual display
											   switch, video mode switch */
        KeyKbdillumtoggle = 278,
        KeyKbdillumdown = 279,
        KeyKbdillumup = 280,
        KeyEject = 281,
        KeySleep = 282,

        KeyApp1 = 283,
        KeyApp2 = 284
    }
}