﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FusionGL.Core.ScriptHost.Tests
{
    [TestClass]
    public class LuaScriptTests
    {
        private const string LuaCode = @"
import('OpenTK','OpenTK.Math')

vec = Vector2(4,5)

function sum(a,b)
    local result = a+b
    return result
end
        ";

        [TestMethod]
        public void CallEventTest()
        {
            var test = new LuaScript(LuaCode);

            var result = test.CallEvent("sum", 10, 10).First();
            Assert.AreEqual("20", result.ToString());
        }
    }
}