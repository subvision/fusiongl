﻿#region LICENSE
//    Copyright 2016 Subvision Studio
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
#endregion
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK;

namespace FusionGL.Core.Tests
{
    [TestClass]
    public class TransformTests
    {
        [TestMethod]
        public void TransformTest()
        {
            var t1 = new Transform(new Vector3(1, 2, 3), new Quaternion(1, 2, 3, 4), Vector3.One);
            Assert.AreEqual(2, t1.Position.Y);
        }

        [TestMethod]
        public void SetParentTransformTest()
        {
            var t1 = new Transform(Vector3.One, Quaternion.Identity, Vector3.One);
            var t2 = new Transform(Vector3.Zero, Quaternion.Identity, Vector3.One);

            t2.SetParentTransform(t1);
            Assert.AreEqual(t1.GetTransformation(), t2.GetTransformation());
        }

        [TestMethod]
        public void GetTransformationTest()
        {
            var t1 = new Transform(Vector3.Zero, Quaternion.Identity, Vector3.One);

            Assert.AreEqual(Matrix4.Identity, t1.GetTransformation());
        }
    }
}